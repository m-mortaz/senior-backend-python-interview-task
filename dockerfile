FROM python:3.10
WORKDIR /
RUN pip install poetry==1.2.2 && poetry config virtualenvs.create false
COPY . .
RUN poetry install --only main

