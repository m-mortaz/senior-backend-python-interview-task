from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "category" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "update_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "name" VARCHAR(128) NOT NULL UNIQUE
);
CREATE INDEX IF NOT EXISTS "idx_category_name_8b0cb9" ON "category" ("name");
CREATE TABLE IF NOT EXISTS "member" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "update_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "username" VARCHAR(128) NOT NULL UNIQUE
);
CREATE INDEX IF NOT EXISTS "idx_member_usernam_1d98a4" ON "member" ("username");
CREATE TABLE IF NOT EXISTS "categorypermission" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "update_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "can_create" BOOL NOT NULL  DEFAULT False,
    "can_update" BOOL NOT NULL  DEFAULT False,
    "can_read" BOOL NOT NULL  DEFAULT False,
    "can_delete" BOOL NOT NULL  DEFAULT False,
    "assigned_by_id" INT NOT NULL REFERENCES "member" ("id") ON DELETE CASCADE,
    "category_id" INT NOT NULL REFERENCES "category" ("id") ON DELETE CASCADE,
    "member_id" INT NOT NULL REFERENCES "member" ("id") ON DELETE CASCADE,
    CONSTRAINT "uid_categoryper_categor_85197b" UNIQUE ("category_id", "member_id")
);
CREATE INDEX IF NOT EXISTS "idx_categoryper_can_cre_f5ad75" ON "categorypermission" ("can_create");
CREATE INDEX IF NOT EXISTS "idx_categoryper_can_upd_5c5d72" ON "categorypermission" ("can_update");
CREATE INDEX IF NOT EXISTS "idx_categoryper_can_rea_a61988" ON "categorypermission" ("can_read");
CREATE INDEX IF NOT EXISTS "idx_categoryper_can_del_4981cb" ON "categorypermission" ("can_delete");
CREATE TABLE IF NOT EXISTS "document" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "update_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "subject" VARCHAR(128) NOT NULL,
    "content" TEXT NOT NULL,
    "auther_id" INT NOT NULL REFERENCES "member" ("id") ON DELETE CASCADE,
    "category_id" INT NOT NULL REFERENCES "category" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "documentpermission" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "update_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "can_create" BOOL NOT NULL  DEFAULT False,
    "can_update" BOOL NOT NULL  DEFAULT False,
    "can_read" BOOL NOT NULL  DEFAULT False,
    "can_delete" BOOL NOT NULL  DEFAULT False,
    "assigned_by_id" INT NOT NULL REFERENCES "member" ("id") ON DELETE CASCADE,
    "document_id" INT NOT NULL REFERENCES "document" ("id") ON DELETE CASCADE,
    "member_id" INT NOT NULL REFERENCES "member" ("id") ON DELETE CASCADE,
    CONSTRAINT "uid_documentper_documen_f6c52d" UNIQUE ("document_id", "member_id")
);
CREATE INDEX IF NOT EXISTS "idx_documentper_can_cre_755bb9" ON "documentpermission" ("can_create");
CREATE INDEX IF NOT EXISTS "idx_documentper_can_upd_059cce" ON "documentpermission" ("can_update");
CREATE INDEX IF NOT EXISTS "idx_documentper_can_rea_35e14d" ON "documentpermission" ("can_read");
CREATE INDEX IF NOT EXISTS "idx_documentper_can_del_d0c627" ON "documentpermission" ("can_delete");"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        """
