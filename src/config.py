from pathlib import Path
from typing import List, Optional

from pydantic import BaseSettings, SecretStr


class Settings(BaseSettings):
    BASE_DIR = Path(__file__).resolve().parent.parent
    PROJECT_NAME: str = "DocumentSystem"
    DEBUG: bool = False

    BACKEND_CORS_ORIGINS: List[str] = ["*"]
    APP_PORT: int = 8080
    DB_USER: str
    DB_PASSWORD: SecretStr
    DB_HOST: str
    DB_PORT: str
    DB_NAME: str

    SENTRY_DSN: Optional[str] = None

    REDIS_HOST: str
    REDIS_PORT: str

    PROMETHEUS_PORT: int = 1000

    UVICORN_WORKERS: int = 100

    class Config:
        case_sensitive = True
        arbitrary_types_allowed = True
        allow_population_by_alias = True
        env_file = ".env"
        env_file_encoding = "utf-8"

    @property
    def log_dict(self):
        return {
            "level": "DEBUG",
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {"simple": {"format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"}},
            "handlers": {
                "file": {
                    "level": "DEBUG",
                    "class": "logging.FileHandler",
                    "filename": "doc_system.log",
                    "formatter": "simple",
                },
                "console": {
                    "class": "logging.StreamHandler",
                    "level": "DEBUG",
                    "formatter": "simple",
                },
            },
            "loggers": {
                "console": {
                    "handlers": ["console"],
                    "propagate": False,
                    "level": "INFO",
                },
                "uvicorn": {"handlers": ["console", "file"], "propagate": False, "level": "INFO"},
                "uvicorn.access": {"handlers": ["console", "file"], "propagate": False, "level": "INFO"},
                "uvicorn.error": {"handlers": ["console", "file"], "propagate": False, "level": "INFO"},
                "db_client": {
                    "handlers": ["console", "file"],
                    "propagate": False,
                    "level": "DEBUG" if self.DEBUG else "ERROR",
                },
                "tortoise": {
                    "handlers": ["console", "file"],
                    "propagate": False,
                    "level": "DEBUG" if self.DEBUG else "ERROR",
                },
                "root": {
                    "handlers": ["console", "file"],
                    "propagate": False,
                    "level": "INFO",
                },
            },
        }


settings = Settings()

TORTOISE_ORM = {
    "connections": {
        "default": {
            "engine": "tortoise.backends.asyncpg",
            "credentials": {
                "database": settings.DB_NAME,
                "host": settings.DB_HOST,
                "port": settings.DB_PORT,
                "user": settings.DB_USER,
                "password": settings.DB_PASSWORD.get_secret_value(),
            },
            "cast": {
                "max_inactive_connection_lifetime": 20.0,
            },
        },
    },
    "apps": {
        "models": {
            "models": ["aerich.models", "src.models"],
            "default_connection": "default",
        },
    },
    "use_tz": False,
    "timezone": "UTC",
}
