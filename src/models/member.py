from tortoise import fields

from src.commons.base_model import AbstractBaseModel
from src.models.permission import CategoryPermission, DocumentPermission


class Member(AbstractBaseModel):
    username = fields.CharField(max_length=128, unique=True, index=True)

    @property
    def is_superuser(self):
        return self.username and self.username == "admin"

    async def has_category_permission(self, category_id: int, permission: str) -> bool:
        cat_perm = await CategoryPermission.get_or_none(member=self, category_id=category_id)
        if cat_perm is None:
            return False
        return getattr(cat_perm, permission, False)

    async def has_direct_doc_permission(self, document_id: int, permission: str) -> bool:
        doc_perm = await DocumentPermission.get_or_none(member=self, document_id=document_id)
        if doc_perm is None:
            return False
        return getattr(doc_perm, permission, False)

    async def upsert_doc_permission(
        self,
        assigned_by_id: int,
        document_id: int,
        can_update: bool,
        can_delete: bool,
        can_create: bool,
        can_read: bool,
    ):
        defaults = {
            "can_update": can_update,
            "can_delete": can_delete,
            "can_create": can_create,
            "can_read": can_read,
            "assigned_by_id": assigned_by_id,
        }
        access_denied = not (can_read or can_create or can_delete or can_update)
        if access_denied:  # delete permission of doc when all permissions have been token from member :)
            doc_perm = await DocumentPermission.get_or_none(member_id=self.id, document_id=document_id)
            await doc_perm.delete()
            return
        await DocumentPermission.update_or_create(member_id=self.id, document_id=document_id, defaults=defaults)

    async def upsert_category_permission(
        self,
        assigned_by_id: int,
        category_id: int,
        can_update: bool,
        can_delete: bool,
        can_create: bool,
        can_read: bool,
    ):
        defaults = {
            "can_update": can_update,
            "can_delete": can_delete,
            "can_create": can_create,
            "can_read": can_read,
            "assigned_by_id": assigned_by_id,
        }
        access_denied = not (can_read or can_create or can_delete or can_update)
        if access_denied:  # delete permission of CATEGORY when all permissions have been token from member :)
            doc_perm = await CategoryPermission.get_or_none(member_id=self.id, category_id=category_id)
            await doc_perm.delete()
            return
        await CategoryPermission.update_or_create(member_id=self.id, category_id=category_id, defaults=defaults)
