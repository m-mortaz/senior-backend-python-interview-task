from tortoise import fields

from src.commons.base_model import AbstractBaseModel


class CategoryPermission(AbstractBaseModel):
    category = fields.ForeignKeyField("models.Category", related_name="category_permissions")
    member = fields.ForeignKeyField("models.Member", related_name="category_permissions")
    assigned_by = fields.ForeignKeyField("models.Member", related_name="category_assigned")

    can_create = fields.BooleanField(index=True, default=False)
    can_update = fields.BooleanField(index=True, default=False)
    can_read = fields.BooleanField(index=True, default=False)
    can_delete = fields.BooleanField(index=True, default=False)

    class Meta:
        unique_together = ["category", "member"]


class DocumentPermission(AbstractBaseModel):
    document = fields.ForeignKeyField("models.Document", related_name="document_permissions")
    member = fields.ForeignKeyField("models.Member", related_name="document_permissions")
    assigned_by = fields.ForeignKeyField("models.Member", related_name="document_assigned")

    can_create = fields.BooleanField(index=True, default=False)
    can_update = fields.BooleanField(index=True, default=False)
    can_read = fields.BooleanField(index=True, default=False)
    can_delete = fields.BooleanField(index=True, default=False)

    class Meta:
        unique_together = ["document", "member"]
