from tortoise import fields

from src.commons.base_model import AbstractBaseModel


class Category(AbstractBaseModel):
    name = fields.CharField(max_length=128, doc="Category name.", index=True, unique=True)

    category_permissions = fields.ReverseRelation["CategoryPermission"]
    documents = fields.ReverseRelation["Document"]


class Document(AbstractBaseModel):
    subject = fields.CharField(max_length=128)
    content = fields.TextField()
    auther = fields.ForeignKeyField("models.Member", related_name="documents", null=False)
    category = fields.ForeignKeyField("models.Category", related_name="documents")

    document_permissions = fields.ReverseRelation["DocumentPermission"]

    class Meta:
        ordering = ["-id"]
