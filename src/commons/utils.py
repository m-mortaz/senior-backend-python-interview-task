import inspect


def get_attr(obj) -> list:
    return [
        {"key": i[0], "value": i[1]}
        for i in inspect.getmembers(obj)
        if not i[0].startswith("_") and not inspect.ismethod(i[1])
    ]
