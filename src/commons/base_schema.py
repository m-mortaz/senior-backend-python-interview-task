from datetime import datetime

from pydantic import BaseModel


class BaseOutSchema(BaseModel):
    id: int
    created_at: datetime
    update_at: datetime
