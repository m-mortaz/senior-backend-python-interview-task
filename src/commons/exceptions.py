from typing import Optional

from fastapi import HTTPException
from starlette import status


class BaseAPIException(HTTPException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = "A server error occurred."
    default_code = "error"
    headers = {}

    def __init__(self, detail: Optional[str] = None):
        super().__init__(status_code=self.status_code, detail=detail or self.default_detail)

    def to_dict(self):
        return {
            "non_field_errors": [
                {
                    "code": self.default_code,
                    "description": self.detail,
                }
            ]
        }


class UnauthorizedAPIException(BaseAPIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Cannot fetch your identity."
    default_code = "user_unauthorized"


class ForbiddenAPIException(BaseAPIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = "Access Denied."
    default_code = "forbidden"


class DuplicatedUsernameAPIException(BaseAPIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Duplicated username."
    default_code = "duplicated_username"


class DuplicatedCategoryNameAPIException(BaseAPIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Duplicated category name."
    default_code = "duplicated_name"


class ObjectNotFoundAPIException(BaseAPIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_code = "not_found"


class PermissionNotFoundException(Exception):
    pass
