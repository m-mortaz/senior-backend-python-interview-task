# from dataclasses import dataclass
#
# from src.commons.utils import get_attr
# from src.commons.exceptions import PermissionNotFoundException
# from src.commons.singleton import Singleton


class PermissionsChoices:
    CREATE = "can_create"
    READ = "can_read"
    UPDATE = "can_update"
    DELETE = "can_delete"


#
# class PermissionConverter(Singleton):
#     id_to_name_mapper = {p["value"].id: p["value"].name.lower() for p in get_attr(PermissionsChoices)}
#     name_to_id_mapper = {p["value"].id: p["value"].name.lower() for p in get_attr(PermissionsChoices)}
#
#     @classmethod
#     def get_id_by_name(cls, name: str) -> int:
#         """
#         Get permission's ID using permission's name.
#         :param name: The permission name.
#         :return: int
#         :raise PermissionNotFoundException if permission name not exists.
#         """
#         try:
#             return cls.id_to_name_mapper[name]
#         except KeyError:
#             raise PermissionNotFoundException
#
#     @classmethod
#     def get_name_by_id(cls, permission_id: int) -> str:
#         """
#         Get permission's ID using permission's name.
#         :param permission_id: permission id
#         :return: str: permission name
#         :raise PermissionNotFoundException if permission name not exists.
#         """
#         try:
#             return cls.name_to_id_mapper[permission_id]
#         except KeyError:
#             raise PermissionNotFoundException
