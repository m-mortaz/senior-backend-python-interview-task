from typing import Optional, Dict

from fastapi import Request, Header, Depends
from tortoise.exceptions import (
    DoesNotExist,
)

from src.commons.data import PermissionsChoices
from src.commons.exceptions import UnauthorizedAPIException, ForbiddenAPIException
from src.models import Member, Document


class BaseAuth:
    """
    Authentication and authorization
    """

    def __init__(self, scope: Optional[str] = None):
        self.member: Member = Member(id=-1, username="Unknown")
        self.scope = scope
        self.request_body: Dict = dict()
        self.username: Optional[str] = None

    async def __call__(self, request: Request, username: str = Header(default=None)):
        try:
            self.request_body = await request.json()
        except Exception:
            pass
        self.username = username
        self.url_doc_id = request.path_params.get("doc_id")
        return await self.auth()

    async def auth(self):
        self.member: Member = await self.authenticate()
        if not self.member.is_superuser:
            await self.authorize()
        return self.member

    async def authenticate(self):
        if self.username is None:
            raise UnauthorizedAPIException
        try:
            return await Member.get(username=self.username)
        except DoesNotExist:
            raise UnauthorizedAPIException

    async def authorize(self):
        """
        Authorize member to access doc
        """
        if self.member is None:
            raise ForbiddenAPIException

        if self.scope == PermissionsChoices.CREATE:
            await self.create_permission()

        if self.scope == PermissionsChoices.DELETE:
            await self.delete_permission()

        if self.scope == PermissionsChoices.READ and self.url_doc_id:
            await self.read_single_permission()

        if self.scope == PermissionsChoices.READ and not self.url_doc_id:
            await self.read_bulk_permission()

        if self.scope == PermissionsChoices.UPDATE:
            await self.update_permission()

    async def create_permission(self):
        category_id = self.request_body.get("category")

        if not await self.member.has_category_permission(category_id=category_id, permission=self.scope):
            raise ForbiddenAPIException(detail="Forbidden create document for category.")

    async def read_single_permission(self):
        doc: Document = await Document.get_or_none(id=self.url_doc_id)

        if not (
            doc
            and (
                await self.member.has_category_permission(category_id=doc.category_id, permission=self.scope)
                or await self.member.has_direct_doc_permission(document_id=self.url_doc_id, permission=self.scope)
            )
        ):
            raise ForbiddenAPIException(detail="Forbidden read the document.")

    async def read_bulk_permission(self):
        pass

    async def delete_permission(self):
        doc: Document = await Document.get_or_none(id=self.url_doc_id)

        if not (
            doc
            and (
                await self.member.has_category_permission(category_id=doc.category_id, permission=self.scope)
                or await self.member.has_direct_doc_permission(document_id=self.url_doc_id, permission=self.scope)
            )
        ):
            raise ForbiddenAPIException(detail="Forbidden delete this document.")

    async def update_permission(self):
        new_category_id = self.request_body.get("category")
        doc: Document = await Document.get_or_none(id=self.url_doc_id)

        if not (
            new_category_id
            and doc
            and (
                (
                    (
                        await self.member.has_category_permission(category_id=doc.category_id, permission=self.scope)
                        and await self.member.has_category_permission(
                            category_id=new_category_id, permission=self.scope
                        )
                    )
                    or await self.member.has_direct_doc_permission(document_id=self.url_doc_id, permission=self.scope)
                )
            )
        ):
            raise ForbiddenAPIException(detail="Forbidden update this document.")


class SuperUserAuth:
    """
    Check if member is superuser or not
    Superuser always has authorization!
    """

    async def __call__(self, request: Request, username: str = Header(default=None)):
        member: Member = await self.authenticate(username)
        if member.is_superuser:
            return member
        raise ForbiddenAPIException

    async def authenticate(self, username: str):
        if username is None:
            raise UnauthorizedAPIException
        try:
            return await Member.get(username=username)
        except DoesNotExist:
            raise UnauthorizedAPIException


# TODO: Use redis to cache, db hit will be fucked us!
Auth = lambda scope: Depends(BaseAuth(scope=scope))  # noqa
IsSuperUserPermissionCheck = lambda: Depends(SuperUserAuth())  # noqa
