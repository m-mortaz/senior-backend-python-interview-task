from tortoise import Model, fields


class AbstractBaseModel(Model):
    id = fields.IntField(pk=True)
    update_at = fields.DatetimeField(auto_now=True)
    created_at = fields.DatetimeField(auto_now_add=True)

    class Meta:
        abstract = True

    class PydanticMeta:
        exclude = ["id", "update_at", "created_at"]

    def __str__(self):
        return self.id
