from typing import Callable

from fastapi import Request, Response
from fastapi.exceptions import RequestValidationError
from fastapi.routing import APIRoute
from sentry_sdk import capture_exception
from starlette import status
from starlette.requests import Request as StarRequest
from starlette.responses import JSONResponse

from src.commons.exceptions import BaseAPIException


class CustomExceptionRouter(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            try:
                return await original_route_handler(request)
            except (BaseAPIException, RequestValidationError) as e:
                raise e
            except Exception as e:
                capture_exception(e)
                raise BaseAPIException() from e

        return custom_route_handler


def assemble_validation_response(details):
    _data = [
        {
            ".".join(err.get("loc", ["unknown"])): {
                "code": err.get("type"),
                "detail": err.get("msg"),
            }
        }
        for err in details
    ]
    data = {k: [d.get(k) for d in _data if d.get(k)] for k in set().union(*_data)}
    return data


async def custom_http_exception_handler(request: StarRequest, exc: BaseAPIException) -> JSONResponse:
    """
    Overwrite rest framework's default http exception handler to coerce error messages to our standard
    """
    return JSONResponse(exc.to_dict(), status_code=exc.status_code, headers=exc.headers or None)


async def custom_validation_exception_handler(request: StarRequest, exc: RequestValidationError) -> JSONResponse:
    """
    Overwrite rest framework's default validation exception handler to coerce error messages to our standard
    """
    data = assemble_validation_response(exc.errors())
    return JSONResponse(data, status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)
