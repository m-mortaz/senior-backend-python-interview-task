from typing import Union

from fastapi import APIRouter, Request
from starlette import status
from starlette.responses import Response

from src.commons.auth import IsSuperUserPermissionCheck
from src.commons.exceptions import ObjectNotFoundAPIException
from src.models import Member, Document, Category
from src.schemas.permission import CreatePermissionInSchema

router = APIRouter()


@router.post(
    "/create/",
)
async def create_permission(
    request: Request,
    payload: CreatePermissionInSchema,
    admin: Member = IsSuperUserPermissionCheck(),
):
    """
    Admin can give access for a user
    """

    member: Union[Member, None] = await Member.get_or_none(id=payload.member_id)
    if not member:
        raise ObjectNotFoundAPIException(detail=f"Member with id {payload.member_id} not exists!")

    if payload.document_id is not None:  # assign doc
        doc: Union[Document, None] = await Document.get_or_none(id=payload.document_id)
        if not doc:
            raise ObjectNotFoundAPIException(detail=f"Document with id {payload.document_id} not exists!")
        # TODO: Use another data type.
        await member.upsert_doc_permission(
            assigned_by_id=admin.id,
            document_id=payload.document_id,
            can_update=payload.can_update,
            can_delete=payload.can_delete,
            can_create=payload.can_create,
            can_read=payload.can_read,
        )

    if payload.category_id is not None:  # assign category
        cat: Union[Category, None] = await Category.get_or_none(id=payload.category_id)
        if not cat:
            raise ObjectNotFoundAPIException(detail=f"Category with id {payload.category_id} not exists!")

        # TODO: Use another data type.
        await member.upsert_category_permission(
            category_id=cat.id,
            assigned_by_id=admin.id,
            can_update=payload.can_update,
            can_delete=payload.can_delete,
            can_create=payload.can_create,
            can_read=payload.can_read,
        )
    return Response(status_code=status.HTTP_204_NO_CONTENT)
