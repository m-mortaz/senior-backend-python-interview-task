from fastapi import APIRouter

from . import document, member, permission, category

APP_ROUTERS = APIRouter()
APP_ROUTERS.include_router(member.router, prefix="/member")
APP_ROUTERS.include_router(document.router, prefix="/document")
APP_ROUTERS.include_router(permission.router, prefix="/permission")
APP_ROUTERS.include_router(category.router, prefix="/category")
