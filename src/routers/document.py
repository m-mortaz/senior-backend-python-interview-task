from typing import List, Dict

from django.db.models import OuterRef
from fastapi import APIRouter, Request, Query
from starlette import status
from starlette.responses import Response
from tortoise.expressions import Q, Subquery, RawSQL, F
from tortoise.functions import Concat

from src.commons.auth import Auth
from src.commons.data import PermissionsChoices
from src.commons.exceptions import ObjectNotFoundAPIException
from src.models import Member, Document, Category
from src.models.permission import CategoryPermission, DocumentPermission
from src.schemas.category import CategoryOutSchema
from src.schemas.document import DocumentInSchema, DocumentOutSchema, ComplexDocumentOutSchema
from src.schemas.member import MemberOutSchema

router = APIRouter()


@router.get("/{doc_id}/", response_model=DocumentOutSchema)
async def read_document(request: Request, doc_id: int, member: Member = Auth(scope=PermissionsChoices.READ)):
    """Retrieve a doc"""
    doc = await Document.get_or_none(id=doc_id).select_related("auther", "category")
    if doc is None:
        raise ObjectNotFoundAPIException("Document not found")
    if member.is_superuser:
        doc = await Document.get_or_none(id=doc_id).select_related("auther", "category")
        if doc is None:
            raise ObjectNotFoundAPIException("Document not found")
        kwargs = {
            "id": doc.id,
            "auther_id": doc.auther.id,
            "auther__username": doc.auther.username,
            "category_id": doc.category.id,
            "category__name": doc.category.name,
            "cat_perms": "t" * 4,
            "doc_perms": "t" * 4,
        }

    else:
        d_sub = DocumentPermission.filter(member=member, document=doc_id).annotate(
            res=Concat(F("can_create"), F("can_read"), F("can_delete"), F("can_update"))
        )

        cat_sub = CategoryPermission.filter(member=member, category=RawSQL("document.category_id")).annotate(
            res=Concat(F("can_create"), F("can_read"), F("can_delete"), F("can_update"))
        )
        kwargs = await (
            Document.filter(id=doc_id)
            .filter(Q(document_permissions__member=member) | Q(category__category_permissions__member=member))
            .annotate(cat_perms=Subquery(cat_sub.values("res")))
            .annotate(doc_perms=Subquery(d_sub.values("res")))
            .values("id", "auther_id", "auther__username", "category_id", "category__name", "cat_perms", "doc_perms")
        )
        if not kwargs:
            raise ObjectNotFoundAPIException("Document not found")
        kwargs = kwargs[0]

    return DocumentOutSchema.load_from_dict(kwargs)


@router.get("/", response_model=List[DocumentOutSchema])
async def read_list_documents(
    request: Request,
    member: Member = Auth(scope=PermissionsChoices.READ),
    page: int = Query(default=0),
    size: int = Query(default=50, le=100, ge=1),
):
    """Get list of doc"""
    # ------------- Sorry dear db, I'm going to kill you! \0/ ------
    docs_qs = Document.all().limit(size).offset(size * page).select_related("category", "auther").order_by("-id")

    if member.is_superuser:
        docs_objs = list(await docs_qs.values("id", "auther_id", "auther__username", "category_id", "category__name"))
        return [
            DocumentOutSchema.load_from_dict(i.update({"cat_perms": "t" * 4, "doc_perms": "t" * 4}) or i)
            for i in docs_objs
        ]

    d_sub = DocumentPermission.filter(member=member, document=RawSQL("document.id")).annotate(
        res=Concat(F("can_create"), F("can_read"), F("can_delete"), F("can_update"))
    )

    cat_sub = CategoryPermission.filter(member=member, category=RawSQL("document.category_id")).annotate(
        res=Concat(F("can_create"), F("can_read"), F("can_delete"), F("can_update"))
    )
    docs_qs = (
        docs_qs.filter(Q(document_permissions__member=member) | Q(category__category_permissions__member=member))
        .annotate(cat_perms=Subquery(cat_sub.values("res")))
        .annotate(doc_perms=Subquery(d_sub.values("res")))
        .values("id", "auther_id", "auther__username", "category_id", "category__name", "cat_perms", "doc_perms")
    )
    docs_objs = await docs_qs

    return [DocumentOutSchema.load_from_dict(i) for i in docs_objs]


@router.delete(
    "/{doc_id}/",
)
async def delete_document(request: Request, doc_id: int, member: Member = Auth(scope=PermissionsChoices.DELETE)):
    return_value = await Document.filter(id=doc_id).delete()
    print(return_value)
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.post("/", response_model=ComplexDocumentOutSchema)
async def create_document(
    request: Request,
    payload: DocumentInSchema,
    member: Member = Auth(scope=PermissionsChoices.CREATE),
):
    if not await Category.filter(id=payload.category).exists():
        raise ObjectNotFoundAPIException(f"Category with id {payload.category} not exist.")
    doc = await Document.create(
        subject=payload.subject, content=payload.content, auther_id=member.id, category_id=payload.category
    )
    return await ComplexDocumentOutSchema.from_tortoise_orm(doc)


@router.put("/{doc_id}/", response_model=ComplexDocumentOutSchema)
async def update_document(
    request: Request, doc_id: int, payload: DocumentInSchema, member: Member = Auth(scope=PermissionsChoices.UPDATE)
):
    # TODO: check for decision for update by other member
    doc = await Document.filter(id=doc_id).update(**payload.dict())
    return await ComplexDocumentOutSchema.from_tortoise_orm(doc)
