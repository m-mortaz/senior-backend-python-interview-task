from typing import List

from fastapi import APIRouter, Request
from tortoise.exceptions import IntegrityError

from src.commons.auth import IsSuperUserPermissionCheck
from src.commons.exceptions import DuplicatedCategoryNameAPIException
from src.models import Member, Category
from src.schemas.category import CategoryOutSchema, CategoryInSchema

router = APIRouter()


@router.post("/", response_model=CategoryOutSchema)
async def create_category(request: Request, payload: CategoryInSchema, member: Member = IsSuperUserPermissionCheck()):
    """Create a category"""
    try:
        category = await Category.create(**payload.dict())
    except IntegrityError:
        raise DuplicatedCategoryNameAPIException
    return category


@router.get("/", response_model=List[CategoryOutSchema])
async def get_all_categories(request: Request, member: Member = IsSuperUserPermissionCheck()):
    return await Category.all()
