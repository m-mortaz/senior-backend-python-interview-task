import logging
from typing import List

from fastapi import APIRouter
from tortoise.exceptions import IntegrityError

from src.commons.auth import IsSuperUserPermissionCheck
from src.commons.exceptions import DuplicatedUsernameAPIException
from src.models import Member
from src.schemas.member import MemberInSchema, MemberOutSchema

router = APIRouter()

logger = logging.getLogger(__name__)


@router.post("/create", response_model=MemberOutSchema, dependencies=[IsSuperUserPermissionCheck()])
async def create_member(payload: MemberInSchema):
    try:
        member = await Member.create(**payload.dict())
    except IntegrityError as e:
        logger.error(e)
        raise DuplicatedUsernameAPIException()
    return member


@router.get("/", response_model=List[MemberOutSchema], dependencies=[IsSuperUserPermissionCheck()])
async def read_list_member():
    members = await Member.all()
    return members
