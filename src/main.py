import logging.config

from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi_pagination import add_pagination
from sentry_sdk import init as initialize_sentry
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.threading import ThreadingIntegration
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise

from src.commons.exceptions import BaseAPIException
from src.config import settings, TORTOISE_ORM
from src.exception_handler import (
    CustomExceptionRouter,
    custom_http_exception_handler,
    custom_validation_exception_handler,
)
from src.routers import APP_ROUTERS

logging.config.dictConfig(settings.log_dict)

logger = logging.getLogger(__name__)


class PhaseChoice:
    PRODUCTION = "production"
    DEVELOP = "develop"
    TEST = "test"


def init_sentry():
    sentry_logging = LoggingIntegration(level=logging.INFO, event_level=logging.ERROR)
    if settings.SENTRY_DSN:
        initialize_sentry(
            dsn=settings.SENTRY_DSN,
            integrations=[sentry_logging, ThreadingIntegration()],
            environment=settings.PHASE,
            debug=settings.DEBUG,
        )


def register_db(starlet_app, phase: str):
    if phase == PhaseChoice.TEST:
        register_tortoise(
            starlet_app,
            db_url="sqlite://:memory:",
            modules={"models": ["aerich.models", "src.models"]},
            generate_schemas=True,
            add_exception_handlers=True,
        )
    elif phase == PhaseChoice.PRODUCTION:
        register_tortoise(
            starlet_app,
            config=TORTOISE_ORM,
            generate_schemas=False,
            add_exception_handlers=False,
        )


async def shutdown_db():
    await Tortoise.close_connections()


def get_application(phase: str = PhaseChoice.PRODUCTION) -> FastAPI:
    _app = FastAPI(title=settings.PROJECT_NAME, debug=settings.DEBUG)
    _app.include_router(APP_ROUTERS, prefix="/v1")
    register_db(_app, phase=phase)
    add_pagination(_app)

    @_app.on_event("startup")
    async def startup():
        from src.models import Member

        await Member.get_or_create(username="admin")

    @_app.on_event("shutdown")
    async def shutdown():
        logger.info("Graceful close of db connections")
        await shutdown_db()

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    if settings.SENTRY_DSN:
        _app.add_middleware(SentryAsgiMiddleware)

    _app.router.route_class = CustomExceptionRouter

    @_app.exception_handler(BaseAPIException)
    async def http_exception(request, exc):
        return await custom_http_exception_handler(request, exc)

    @_app.exception_handler(RequestValidationError)
    async def validation_exception(request, exc):
        return await custom_validation_exception_handler(request, exc)

    return _app
