from typing import Optional, Any, Type

from pydantic import BaseModel


class CreatePermissionInSchema(BaseModel):
    document_id: Optional[int] = None
    category_id: Optional[int] = None
    member_id: int
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool

    @classmethod
    def validate(cls, value: dict[str, Any]):
        assert (value.get("document_id") is None) != (
            value.get("category_id") is None
        ), "Use one of document_id and category_id"
        return super().validate(value=value)
