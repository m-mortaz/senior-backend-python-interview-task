from typing import List

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from src.commons.base_schema import BaseOutSchema
from src.models import Member

MemberInSchema = pydantic_model_creator(Member, exclude_readonly=True)


class MemberOutSchema(BaseOutSchema):
    username: str

    class Config:
        orm_mode = True


class SimpleAuther(BaseModel):
    id: int
    username: str
