from typing import Dict

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from src.models import Document
from src.schemas.category import SimpleCategory
from src.schemas.member import SimpleAuther


class DocumentOutSchema(BaseModel):
    id: int
    auther: SimpleAuther
    category: SimpleCategory
    can_create: bool
    can_update: bool
    can_read: bool
    can_delete: bool

    @classmethod
    def load_from_dict(cls, data: Dict):
        cat_can_create, cat_can_read, cat_can_delete, cat_can_update = data.get("cat_perms") or "ffff"
        doc_can_create, doc_can_read, doc_can_delete, doc_can_update = data.get("doc_perms") or "ffff"

        def get_bool(v: str) -> bool:
            return v == "t"

        return cls(
            id=data.get("id"),
            auther=SimpleAuther(id=data.get("auther_id"), username=data.get("auther__username")),
            category=SimpleCategory(id=data.get("category_id"), name=data.get("category__name")),
            can_read=get_bool(cat_can_read) or doc_can_read,
            can_update=get_bool(cat_can_update) or get_bool(doc_can_update),
            can_delete=get_bool(cat_can_delete) or get_bool(doc_can_delete),
            can_create=get_bool(cat_can_create) or get_bool(doc_can_create),
        )


ComplexDocumentOutSchema = pydantic_model_creator(Document, exclude=("auther", "category"))


class DocumentInSchema(BaseModel):
    subject: str
    content: str
    category: int
