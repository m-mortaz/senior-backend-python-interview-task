from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from src.commons.base_schema import BaseOutSchema
from src.models import Category

CategoryInSchema = pydantic_model_creator(Category, exclude_readonly=True)


class CategoryOutSchema(BaseOutSchema):
    name: str


class SimpleCategory(BaseModel):
    id: int
    name: str
