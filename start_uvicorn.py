import logging

import uvicorn

from src.config import settings
from src.main import get_application, PhaseChoice

app = get_application(phase=PhaseChoice.PRODUCTION)

if __name__ == "__main__":
    config = uvicorn.Config(
        app,
        host="0.0.0.0",
        port=settings.APP_PORT,
        log_config=settings.log_dict,
        workers=settings.UVICORN_WORKERS,
    )
    logger = logging.getLogger(__name__)
    server = uvicorn.Server(config)
    server.run()
