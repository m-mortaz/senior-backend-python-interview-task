import asyncio

import pytest
from fastapi.testclient import TestClient
from src.models import Member


@pytest.mark.parametrize("username", ["member1", "member2"])
def test_create_user(client: TestClient, event_loop: asyncio.AbstractEventLoop, username, faker):
    # -------- Admin Create Member ---------
    response = client.post("/v1/member/create", json={"username": username}, headers={"username": "admin"})
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["username"] == username
    assert "id" in data
    user_id = data["id"]

    async def get_user_by_db():
        member = await Member.get(id=user_id)
        return member

    user_obj: Member = event_loop.run_until_complete(get_user_by_db())
    assert user_obj.id == user_id
    assert user_obj.username == username

    # ---------- Normal Member Can't Create Another Member -------
    response = client.post("/v1/member/create", json={"username": faker.name()}, headers={"username": username})
    assert response.status_code == 403
