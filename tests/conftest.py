import asyncio
import time
from typing import Generator, Callable, List

import pytest
from faker import Faker
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer

from src.config import TORTOISE_ORM
from src.main import get_application, PhaseChoice
from src.models import Member

app = get_application(phase=PhaseChoice.TEST)

loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)


@pytest.fixture(scope="module")
def client() -> Generator:
    initializer(
        modules=TORTOISE_ORM["apps"]["models"]["models"], app_label="models", loop=loop, db_url="sqlite://:memory:"
    )
    with TestClient(app) as c:
        yield c
    finalizer()


@pytest.fixture(scope="package")
def event_loop() -> Generator:
    yield loop


@pytest.fixture(scope="module")
def admin(event_loop: asyncio.AbstractEventLoop) -> Member:
    async def get_admin() -> Member:
        return await Member.get(username="admin")

    return event_loop.run_until_complete(get_admin())


@pytest.fixture(scope="package")
def faker() -> Faker:
    return Faker()


@pytest.fixture(scope="package")
def fresh_member_func(faker) -> Callable[[int], List[Member]]:
    def create_multiple_fresh_members(n) -> List[Member]:
        async def runner():
            async def _create_fresh_member():
                return await Member.create(faker.name() + str(time.time_ns()))

            return await asyncio.gather(*[_create_fresh_member() for i in range(n)])

        return loop.run_until_complete(runner())

    return create_multiple_fresh_members
