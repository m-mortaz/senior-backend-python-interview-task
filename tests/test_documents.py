import asyncio

import pytest
from fastapi.testclient import TestClient

from src.models import Category, Member


@pytest.mark.parametrize(["doc_name", "cat_name"], [["doc1", "cat1"], ["doc2", "cat2"]])
def test_create_document(client: TestClient, event_loop: asyncio.AbstractEventLoop, doc_name, cat_name, faker, admin):
    # -------- Admin Can Create Category ---------
    response = client.post("/v1/category/", json={"name": cat_name}, headers={"username": admin.username})
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["name"] == cat_name
    assert "id" in data
    cat_id = data["id"]

    async def get_cat_by_db():
        return await Category.get(id=cat_id)

    doc_obj: Category = event_loop.run_until_complete(get_cat_by_db())
    assert doc_obj.id == cat_id
    assert doc_obj.name == cat_name

    # ---------- Normal Member Can't Create Category -------
    async def create_new_member():
        return await Member.create(username=faker.name())

    member: Member = event_loop.run_until_complete(create_new_member())
    response = client.post("/v1/category/", json={"name": cat_name}, headers={"username": member.username})
    assert response.status_code == 403

    # -------- Admin Can Create Document ---------
    data = {"subject": doc_name, "content": doc_name, "category": cat_id}
    response = client.post("/v1/document/", json=data, headers={"username": admin.username})

    assert response.status_code == 200, response.text
    data = response.json()
    assert data["subject"] == doc_name
    assert data["content"] == doc_name
    assert data["category_id"] == cat_id
    assert data["auther_id"] == admin.id

    # ------- Permission on doc -----
    response = client.get(f"/v1/document/{doc_obj.id}", headers={"username": admin.username})
    assert response.status_code == 200
    data = response.json()
    assert data["can_create"] is True
    assert data["can_delete"] is True
    assert data["can_update"] is True
    assert data["can_read"] is True

    response = client.get(f"/v1/document/{doc_obj.id}", headers={"username": member.username})
    assert response.status_code == 403

    data = {
        "category_id": cat_id,
        "member_id": member.id,
        "can_read": True,
        "can_update": False,
        "can_delete": False,
        "can_create": False,
    }
    response = client.post("/v1/permission/create", json=data, headers={"username": admin.username})
    assert response.status_code == 204, response.text

    # bah blah blah :)
