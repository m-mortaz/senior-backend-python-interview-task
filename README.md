# Document Permission System

A system that manages the documents of a company. In this system members can access or modify the documents based on
their permissions.

## Swagger

Run project then brows [http://localhost:port/docs](http://localhost:PORT/docs) (set port from .env).
default development port: 8080
default production (docker) port: 8011

## Auth

Default superuser will be created by app automatically with username=admin.
**Note**: We skip authorization in this system and only validate users by `username` that exists in the
request's `headers`.

## Dependencies

- Python 3.10.x
- FastAPI
- Tortoise-ORM
- Aerich
- Postgresql
- Redis

## Run

### Development

1. Create `.env` using `base.env` and set values for dbm redis and etc.
2. Install [Poetry](https://python-poetry.org/) and install requirements:

```shell
pip3 install poetry
poetry install
poetry shell
```

4. To run in development mode:

```shell
python start_uvicorn.py
```

### Production

```shell
docker compose up
```

## Test

at the root of project run:

```shell
pytest
```

## pre-commit

To activate pre-commit, once run:

```shell
pre-commit install
```

## UML Diagram
Here is the database UML diagram:
![alt uml-diagram](docs/uml_diagram.png)
